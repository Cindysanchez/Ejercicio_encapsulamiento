
package sistema_notas;

public class Sistema_Notas {
    
    public static void main(String[] args) {
        Clase fis = new Clase();
        fis.Fisica();
        Nota nf = new Nota();
        nf.Notas();
        
        System.out.println("Las notas que ingreso son:");
        System.out.println("NOTA 1        -  "+nf.getnota_1());
        System.out.println("NOTA 2        -  "+nf.getnota_2());
        System.out.println("NOTA QUIZ     -  "+nf.getquiz());
        System.out.println("NOTA TRABAJO  -  "+nf.gettrabajo());
        System.out.println("NOTA 4        -  "+nf.getnota_4());
        /*  nf.setnota1(0);
            nf.setnota2(0);
            nf.setquiz(0);
            nf.settrabajo(0);
            nf.setnota4(0);
        */
        System.out.println("Las notas que modifico son:");
        System.out.println("NOTA 1        -"+" ----> "+nf.getnota_1c());
        System.out.println("NOTA 2        -"+" ----> "+nf.getnota_2c());
        System.out.println("NOTA QUIZ     -"+" ----> "+nf.getquizc());
        System.out.println("NOTA TRABAJO  -"+" ----> "+nf.gettrabajoc());
        System.out.println("NOTA 4        -"+" ----> "+nf.getnota_4c());
      
        Clase  mat = new Clase ();
        mat.Matematicas();
        Nota nm = new Nota();
        nm.Notas();
         System.out.println("Las notas que ingreso son:");
        System.out.println("NOTA 1        -  "+nm.getnota_1());
        System.out.println("NOTA 2        -  "+nm.getnota_2());
        System.out.println("NOTA QUIZ     -  "+nm.getquiz());
        System.out.println("NOTA TRABAJO  -  "+nm.gettrabajo());
        System.out.println("NOTA 4        -  "+nm.getnota_4());
       
        System.out.println("Las notas que modifico son:");
        System.out.println("NOTA 1        -"+" ----> "+nm.getnota_1c());
        System.out.println("NOTA 2        -"+" ----> "+nm.getnota_2c());
        System.out.println("NOTA QUIZ     -"+" ----> "+nm.getquizc());
        System.out.println("NOTA TRABAJO  -"+" ----> "+nm.gettrabajoc());
        System.out.println("NOTA 4        -"+" ----> "+nm.getnota_4c());
        
        Clase  qui = new Clase ();
        qui.Quimica();
        Nota nq = new Nota();
        nq.Notas();
         System.out.println("Las notas que ingreso son:");
        System.out.println("NOTA 1        -  "+nq.getnota_1());
        System.out.println("NOTA 2        -  "+nq.getnota_2());
        System.out.println("NOTA QUIZ     -  "+nq.getquiz());
        System.out.println("NOTA TRABAJO  -  "+nq.gettrabajo());
        System.out.println("NOTA 4        -  "+nq.getnota_4());
  
        System.out.println("Las notas que modifico son:");
        System.out.println("NOTA 1        -"+" ----> "+nq.getnota_1c());
        System.out.println("NOTA 2        -"+" ----> "+nq.getnota_2c());
        System.out.println("NOTA QUIZ     -"+" ----> "+nq.getquizc());
        System.out.println("NOTA TRABAJO  -"+" ----> "+nq.gettrabajoc());
        System.out.println("NOTA 4        -"+" ----> "+nq.getnota_4c());
        
        Clase es = new Clase ();
        es.Español();
        Nota ne = new Nota();
        ne.Notas();
         System.out.println("Las notas que ingreso son:");
        System.out.println("NOTA 1        -  "+ne.getnota_1());
        System.out.println("NOTA 2        -  "+ne.getnota_2());
        System.out.println("NOTA QUIZ     -  "+ne.getquiz());
        System.out.println("NOTA TRABAJO  -  "+ne.gettrabajo());
        System.out.println("NOTA 4        -  "+ne.getnota_4());
      
        System.out.println("Las notas que modifico son:");
        System.out.println("NOTA 1        -"+" ----> "+ne.getnota_1c());
        System.out.println("NOTA 2        -"+" ----> "+ne.getnota_2c());
        System.out.println("NOTA QUIZ     -"+" ----> "+ne.getquizc());
        System.out.println("NOTA TRABAJO  -"+" ----> "+ne.gettrabajoc());
        System.out.println("NOTA 4        -"+" ----> "+ne.getnota_4c());
        
        
    }
}
